FROM openjdk:11
RUN ls -la 
#FROM httpd:2.4
#WORKDIR /app
#RUN mv install
ADD target/demo-0.0.1-SNAPSHOT.jar demo-0.0.1-SNAPSHOT.jar
EXPOSE 8443
RUN chmod 777 demo-0.0.1-SNAPSHOT.jar
CMD ['java -jar ','demo-0.0.1-SNAPSHOT.jar']