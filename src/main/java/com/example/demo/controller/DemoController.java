package com.example.demo.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*",methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/demo")
public class DemoController {

    @GetMapping("/")
    public String inicio(){
        return "Hola";
    }

}
